package com.tatiana.fruteria.gui;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLDataException;
import java.sql.SQLException;

public class Controlador implements ActionListener, TableModelListener {

     private Vista vista;
     private Modelo modelo;

    private enum  modoEstado{conectado,desconectado};
    private modoEstado estado;

    public Controlador(Vista vista, Modelo modelo){
        this.modelo=modelo;
        this.vista=vista;
        estado= modoEstado.desconectado;

        iniciarTabla();
        iniciarTabla1();
        addActionListener(this);
        addTableModelListeners(this);

    }
    private void addActionListener(ActionListener listener) {
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.btnfrutasTipo.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);

    }
    private void addTableModelListeners(TableModelListener listener){
        vista.dtm.addTableModelListener(listener);
        vista.dtm1.addTableModelListener(listener);

    }
    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() ==TableModelEvent.UPDATE) {
            System.out.println("actualizada");
            int filaModicada=e.getFirstRow();

            try {
                modelo.modificarFruta((Integer)vista.dtm.getValueAt(filaModicada,0),
                        (String)vista.dtm.getValueAt(filaModicada,1),
                        (String)vista.dtm.getValueAt(filaModicada,2),
                        (String)vista.dtm.getValueAt(filaModicada,3),
                        (String)vista.dtm.getValueAt(filaModicada,4),
                        (String)vista.dtm.getValueAt(filaModicada,5),
                        (java.sql.Timestamp)vista.dtm.getValueAt(filaModicada,6));
                vista.lblAccion.setText("Columna actualizada");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando= e.getActionCommand();

        switch (comando){
            case "Frutas por Nombre":
                try {
                    modelo.frutasPorNombre();
                    cargarFilas1(modelo.obtenerDatos1());
                }catch (SQLDataException e1){
                    e1.printStackTrace();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "CrearTablaFrutas":
                try {
                    modelo.crearTablaFrutas();
                    vista.lblAccion.setText("Tabla frutas creada");
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;

            case "Nuevo":
                try {
                    modelo.insertarFrutas(vista.txtNombre.getText(), vista.txtTipo.getText(), vista.txtOrigen.getText(), vista.txtClasificacion.getText(), vista.txtKilos.getText(), vista.dateTimePicker.getDateTimePermissive());
                    cargarFila(modelo.obtenerDatos());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "Buscar":
                break;
            case "Eliminar":
                try {
                    int filaBorrar=vista.tabla.getSelectedRow();
                    int idBorrar=(Integer) vista.dtm.getValueAt(filaBorrar,0);
                    modelo.eliminarFruta(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Fila Eliminada");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;
            case "Salir":
                System.exit(0);
                break;

            case "Conectar":
                if (estado== modoEstado.desconectado)  {
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado= modoEstado.conectado;
                        cargarFila(modelo.obtenerDatos());
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de conexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }

                } else {
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado= modoEstado.desconectado;
                        vista.lblAccion.setText("Desconectado");
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de desconexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }
                }
                break;
        }

    }


    private void iniciarTabla() {
        String[]headers={"id","nombre","tipo","origen","clasificacion","kilos","fecha_caducidad"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    private void cargarFila(ResultSet obtenerDatos) throws SQLException {
        Object[] fila = new Object[7];
        vista.dtm.setRowCount(0);
        while (obtenerDatos.next()) {
            fila[0] = obtenerDatos.getObject(1);
            fila[1] = obtenerDatos.getObject(2);
            fila[2] = obtenerDatos.getObject(3);
            fila[3] = obtenerDatos.getObject(4);
            fila[4] = obtenerDatos.getObject(5);
            fila[5] = obtenerDatos.getObject(6);
            fila[6] = obtenerDatos.getObject(7);

            vista.dtm.addRow(fila);
        }
        if (obtenerDatos.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(obtenerDatos.getRow() + " FILAS CARGADAS");
        }
    }
    private void iniciarTabla1() {
        String[] headers={"Cuantos","tipo"};
        vista.dtm1.setColumnIdentifiers(headers);
    }


    private void cargarFilas1(ResultSet resultSet) throws SQLException {
      Object[] fila= new Object[7];
      vista.dtm1.setRowCount(0);
      while (resultSet.next()){
          fila[0]=resultSet.getObject(1);
          fila[1]=resultSet.getObject(2);
          fila[2]=resultSet.getObject(3);
          fila[3]=resultSet.getObject(4);
          fila[4]=resultSet.getObject(5);
          fila[5]=resultSet.getObject(6);
          fila[6]=resultSet.getObject(7);

          vista.dtm1.addRow(fila);
      }
      if (resultSet.last()){
          vista.lblAccion.setVisible(true);
          vista.lblAccion.setText(resultSet.getRow()+"FILAS CARGADAS");
      }
    }
  }
