package com.tatiana.fruteria.gui;

import java.sql.*;
import java.time.LocalDateTime;

public class Modelo {
    private Connection conexion;

    public  void  crearTablaFrutas() throws SQLException {
        conexion= null;
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/fruteria","root","");

        String sentenciaSql="call crearTablaFrutas()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    public void conectar() throws SQLException {
        conexion=null;
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/fruteria","root","");

    }
    public void desconectar() throws SQLException {
        conexion.close();
        conexion=null;
    }
    public ResultSet obtenerDatos() throws SQLException {
        if (conexion==null){
            return null;
        }
        if (conexion.isClosed()){
            return null;
        }
        String consulta= "SELECT * FROM frutas";
        PreparedStatement sentencia= null;
        sentencia= conexion.prepareStatement(consulta);
        ResultSet resultado= sentencia.executeQuery();
        return resultado;

    }

    public ResultSet obtenerDatos1() throws SQLException {
        if (conexion== null){
            return null;
        }
        if (conexion.isClosed()){
            return null;
        }
        String consulta= "SELECT * FROM frutas";
        PreparedStatement sentencia= null;
        sentencia= conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;

    }

    public int insertarFrutas(String nombre,String tipo,String origen, String clasificacion, String kilos, LocalDateTime fecha_caducidad) throws SQLException {
        if (conexion == null)
            return -1;
        if (conexion.isClosed())
            return -2;
        String consulta = "INSERT INTO frutas(nombre, tipo, origen, clasificacion, kilos, fecha_caducidad)" +
                "VALUES (?,?,?,?,?,?)";

        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, nombre);
        sentencia.setString(2, tipo);
        sentencia.setString(3, origen);
        sentencia.setString(4, clasificacion);
        sentencia.setString(5, kilos);
        sentencia.setTimestamp(6, Timestamp.valueOf(String.valueOf(Timestamp.valueOf(fecha_caducidad))));

        int numeroRegistros = sentencia.executeUpdate();

        if (sentencia != null) {
            sentencia.close();
        }
        return numeroRegistros;
    }

    public int eliminarFruta(int id) throws SQLException {
       if (conexion== null)
           return -1;
       if (conexion.isClosed())
           return -2;
       String consulta= "DELETE FROM frutas WHERE id=?";
       PreparedStatement sentencia= null;
       sentencia= conexion.prepareStatement(consulta);
       sentencia.setInt(1,id);

       int resultado= sentencia.executeUpdate();
       if (sentencia!= null){
           sentencia.close();
       }
       return resultado;
    }

    public int modificarFruta(int id, String nombre, String tipo, String origen, String clasificacion, String kilos, Timestamp fecha) throws SQLException {
        if (conexion== null)
            return -1;
        if (conexion.isClosed())
            return -2;
        String consulta= "UPDATE frutas SET nombre=?,tipo=?, " +
                "origen=?, clasificacion=?, kilos=?, fecha_caducidad=? WHERER id=?" ;

        PreparedStatement sentencia= null;
        sentencia= conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,tipo);
        sentencia.setString(3,origen);
        sentencia.setString(4,clasificacion);
        sentencia.setString(5,kilos);
        sentencia.setTimestamp(6,fecha);
        sentencia.setInt(7,id);

        int resultado= sentencia.executeUpdate();
        if (sentencia!= null){
            sentencia.close();
        }
        return resultado;

    }

    public ResultSet buscarFruta(String nombre )throws SQLException{
        if (conexion==null){
            return null;
        }

        if (conexion.isClosed()){
            return null;
        }

        String consulta = "SELECT * FROM fruteria where nombre =?";
        PreparedStatement sentencia=null;

        sentencia =conexion.prepareStatement(consulta);

        sentencia.setString(1,(nombre));

        ResultSet resultado=sentencia.executeQuery();

        return resultado;

    }

    public void frutasPorNombre() throws SQLException {
        String sentenciaSql= "call mostrarFrutasNombre()";
        CallableStatement procedimiento= null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }
}
